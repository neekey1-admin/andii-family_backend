const {get_event_list_impl} = require('./event'); 
const {booking_event_impl} = require('./booking');
module.exports.get_event_list = async (event) => {

    return get_event_list_impl(event.pathParameters.facility_id);
}

module.exports.booking_event = async (event) => {

    return booking_event_impl(event.body);

}
