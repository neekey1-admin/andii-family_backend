# Andii Backend

AndiiのBackendを担当する。

## フォルダ構造

基本的に機能別にフォルダを分けている。  
Authなら認証・認可関連  

## デプロイ方法

`yarn run deploy`  
を使用することで、Stagingサーバにデプロイする。

`yarn run deploy:production`  
を使用することで、Productionサーバにデプロイする。(要注意！)
