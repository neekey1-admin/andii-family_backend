'use strict';
const {Client} = require('pg');

module.exports.booking_event_impl = async (event) => {
  const body = JSON.parse(event);
  const user_id = body.user_id;
  const event_id = body.event_id;
  const fee = body.fee;
  const is_vaild_param = typeof user_id === "number" && typeof event_id === "number" && typeof fee === "number";
  if(!is_vaild_param){
    return {
      statusCode: 400,
      body: JSON.stringify({
        status: "Bad request"
      })
    } 
  }

  //登録
  
  //PGクライアントを開く
  const client = new Client({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT
  })
  client.connect();

  const query = {
    text: "INSERT INTO event_booking (facility_user_id, event_id, fee) VALUES ($1,$2,$3)",
    values: [user_id, event_id, fee]
  }

  try {
    const res = await client.query(query);
  } catch (error){
    return {
      statusCode : 500,
      body: JSON.stringify({
        status: "Internal Server Error"
      })
    }
  }
  //response
  return {
    statusCode: 200,
    body: JSON.stringify({
      status: "OK"
    })
  }
};
