

module.exports.signup = async (event) => {

    const body = JSON.parse(event.body);
    
    // バリデーションを行う。
    if(
        typeof body.email === 'string' && 
        typeof body.password === 'string' && 
        typeof body.phone === 'string' && 
        typeof body.name_sei === 'string' &&
        typeof body.name_mei === 'string' &&
        typeof body.name_sei_yomi === 'string' &&
        typeof body.name_mei_yomi === 'string' &&
        typeof body.customer_number === 'string'
    ){
        //do nothing
    }
    else
    {
        console.log(body)
        return {
            "statusCode": 400,
            "body": `Bad Request body:${JSON.stringify(body)}`
        }
    }
    // 登録済みユーザかどうか調べる

    // その次にサインアップを行う。

    return {
        "statusCode": 200,
        "body": "OK"
    }
}