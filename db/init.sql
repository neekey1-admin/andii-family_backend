set client_encoding = 'UTF8';
\c andii

CREATE TYPE delete_flag_enum AS ENUM('Active', 'Deleted');
CREATE TABLE IF NOT EXISTS facility_type_master(
facility_type_id SERIAL NOT NULL,
facility_type_name VARCHAR(255) NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(facility_type_id)
);

INSERT INTO facility_type_master (
    facility_type_name
) VALUES ('デイサービス'),('有料老人ホーム'),('ホスピス');

CREATE TABLE IF NOT EXISTS facility_master(
facility_id SERIAL NOT NULL,
facility_name VARCHAR(255) NOT NULL,
facility_place_name VARCHAR(255) NOT NULL,
facility_lat NUMERIC NOT NULL,
facility_lon NUMERIC NOT NULL,
facility_postalcode VARCHAR(8) NOT NULL,
facility_address VARCHAR(511) NOT NULL,
facility_type_id INTEGER NOT NULL,
facility_number VARCHAR(255),
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(facility_id),
FOREIGN KEY(facility_type_id) REFERENCES facility_type_master(facility_type_id)
);

INSERT INTO facility_master(
    facility_name,
    facility_place_name,
    facility_lat,
    facility_lon,
    facility_postalcode,
    facility_address,
    facility_type_id,
    facility_number
) VALUES (
    'テスト施設',
    'テスト施設デイケア',
    22.222222,
    22.222222,
    '333-0000',
    'テスト県テスト市テスト町1-1-1',
    1,
    '22222222'
);

CREATE TABLE IF NOT EXISTS employee_class_master(
employee_class_id SERIAL NOT NULL,
class_name VARCHAR(255) NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(employee_class_id)
);

INSERT INTO employee_class_master (
    class_name
) VALUES ('施設長'),('介護師/看護師'),('生活相談員');


CREATE TABLE IF NOT EXISTS employee_type_master(
employee_type_id SERIAL NOT NULL,
class_type_name VARCHAR(255) NOT NULL,
is_kanamic_login BOOLEAN NOT NULL DEFAULT 't',
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(employee_type_id)
);


INSERT INTO employee_type_master (
    class_type_name,
    is_kanamic_login
) VALUES ('介護師', 't'),('看護師', 't'),('生活相談員', 'f');


CREATE TABLE IF NOT EXISTS facility_employee(
employee_id SERIAL NOT NULL,
facility_id INTEGER NOT NULL,
employee_class_id INTEGER NOT NULL,
employee_name VARCHAR(255) NOT NULL,
employee_postalcode VARCHAR(8) NOT NULL,
employee_address VARCHAR(511) NOT NULL,
employee_email VARCHAR(255),
employee_phone VARCHAR(31) NOT NULL,
employee_emergency_phone VARCHAR(31) NOT NULL,
employee_type_id INTEGER NOT NULL,
employee_kanamic_id VARCHAR(31),
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(employee_id),
FOREIGN KEY(facility_id) REFERENCES facility_master(facility_id),
FOREIGN KEY(employee_class_id) REFERENCES employee_class_master(employee_class_id),
FOREIGN KEY(employee_type_id) REFERENCES employee_type_master(employee_type_id)
);

INSERT INTO facility_employee (
    facility_id,
    employee_class_id,
    employee_name,
    employee_postalcode,
    employee_address,
    employee_email,
    employee_phone,
    employee_emergency_phone,
    employee_type_id,
    employee_kanamic_id
) VALUES (
    1,
    1,
    '河野太郎',
    '222-2222',
    '埼玉県戸田市本町',
    'kouno_taro@caregiver.co.jp',
    '08077778888',
    '09077722881',
    1,
    '23322323232'
);

CREATE TABLE IF NOT EXISTS event_type_master(
event_type_id SERIAL NOT NULL,
event_type_name VARCHAR(255) NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(event_type_id)
);

INSERT INTO event_type_master (
    event_type_name
) VALUES(
    'もみほぐし'
),(
    '生活サポート'
),(
    '理美容'
),(
    '一般イベント'
)
;

CREATE TABLE IF NOT EXISTS event(
event_id SERIAL NOT NULL,
facility_id INTEGER NOT NULL,
employee_id INTEGER NOT NULL,
event_name VARCHAR(255) NOT NULL,
event_detail TEXT NOT NULL,
event_start TIMESTAMP,
event_end TIMESTAMP,
event_capacity INTEGER,
event_fee INTEGER,
event_type_id INTEGER NOT NULL,
event_img INTEGER NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(event_id),
FOREIGN KEY(facility_id) REFERENCES facility_master(facility_id),
FOREIGN KEY(employee_id) REFERENCES facility_employee(employee_id),
FOREIGN KEY(event_type_id) REFERENCES event_type_master(event_type_id)
);

INSERT INTO event (
    facility_id,
    employee_id,
    event_name,
    event_detail,
    event_start,
    event_end,
    event_capacity,
    event_fee,
    event_type_id,
    event_img
) VALUES (
    1,
    1,
    'BBQ',
    'BBQします！',
    current_timestamp + interval '3 day',
    current_timestamp + interval '6 day',
    5,
    1000,
    4,
    1
),(
    1,
    1,
    '理美容',
    '理美容',
    current_timestamp,
    'tomorrow',
    5,
    1000,
    3,
    1
);

CREATE TABLE IF NOT EXISTS user_type_master(
user_type_id SERIAL NOT NULL,
user_type_name VARCHAR(255) NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(user_type_id)
);

INSERT INTO user_type_master (
    user_type_name
) VALUES (
    '一般ユーザ'
);

CREATE TABLE IF NOT EXISTS facility_user_family(
family_id SERIAL NOT NULL,
family_name VARCHAR(255) NOT NULL,
family_postalcode VARCHAR(255) NOT NULL,
family_address VARCHAR(511) NOT NULL,
family_phone VARCHAR(31) NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(family_id)
);

INSERT INTO facility_user_family (
    family_name,
    family_postalcode,
    family_address,
    family_phone
) VALUES (
    '家族二郎',
    '333-3333',
    '東京都XX区YY',
    '07024443333'
);

CREATE TYPE user_bill_type_enum AS ENUM('user', 'family');
CREATE TABLE IF NOT EXISTS facility_user(
facility_user_id SERIAL NOT NULL,
facility_id INTEGER NOT NULL,
user_type_id INTEGER NOT NULL,
users_family_id INTEGER NOT NULL,
user_name VARCHAR(255) NOT NULL,
user_yomi VARCHAR(255),
user_postalcode VARCHAR(255) NOT NULL,
user_address VARCHAR(255) NOT NULL,
user_phone VARCHAR(255) NOT NULL,
user_emergency_phone VARCHAR(255) NOT NULL,
user_kanamic_id VARCHAR(255) NOT NULL,
user_bill_type user_bill_type_enum NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
PRIMARY KEY(facility_user_id),
FOREIGN KEY(facility_id) REFERENCES facility_master(facility_id),
FOREIGN KEY(user_type_id) REFERENCES user_type_master(user_type_id),
FOREIGN KEY(users_family_id) REFERENCES facility_user_family(family_id)
);

INSERT INTO facility_user (
    facility_id,
    user_type_id,
    users_family_id,
    user_name,
    user_yomi,
    user_postalcode,
    user_address,
    user_phone,
    user_emergency_phone,
    user_kanamic_id,
    user_bill_type
) VALUES (
    1,
    1,
    1,
    '本人太郎',
    'ホンニンタロウ',
    '444-2222',
    'XX県YY市ZZ町',
    '04244339292',
    '02223332222',
    '02020202020',
    'user'
);

CREATE TYPE booking_state_enum AS ENUM('Booking', 'Activated', 'Done', 'Canceled', 'Deleted');
CREATE TABLE IF NOT EXISTS event_booking(
register_id SERIAL NOT NULL,
facility_user_id INTEGER NOT NULL,
event_id INTEGER NOT NULL,
fee INTEGER NOT NULL,
booking_state booking_state_enum NOT NULL DEFAULT 'Booking',
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(register_id),
FOREIGN KEY(facility_user_id) REFERENCES facility_user(facility_user_id),
FOREIGN KEY(event_id) REFERENCES event(event_id)
);

INSERT INTO event_booking (
    facility_user_id,
    event_id,
    fee
) VALUES (
    1,
    1,
    1000
) ;

CREATE TABLE IF NOT EXISTS event_reporting(
register_id INTEGER NOT NULL,
report TEXT NOT NULL,
create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
delete_flag delete_flag_enum NOT NULL DEFAULT 'Active',
FOREIGN KEY(register_id) REFERENCES event_booking(register_id)
);

INSERT INTO event_reporting (
    register_id,
    report
) VALUES (
    1,
    '元気なご様子でした'
);