'use strict';
const {Client} = require('pg');

module.exports.get_event_list = async (event) => {
  const facility_id = Number(event.pathParameters.facility_id)
  //バリデーションチェック
  if (isNaN(facility_id)){
    console.log(event.pathParameters.facility_id)
    return {
      statusCode: 400,
      body: "Bad Request"
    }
  }
  //PGクライアントを開く
  const client = new Client({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT
  })
  client.connect();
  

  //現在開催中のイベントを取得する

  const query = {
    text: "SELECT event_id, event_name, event_fee, event_start, event_end, event_img FROM event WHERE facility_id=$1 AND create_time < current_timestamp	AND event_start > current_timestamp + interval '24 hours'",
    values: [facility_id]
  }
  try{
    const res = await client.query(query);
    
    const response = res.rows.map((e, i)=>{
      return {
        title: e.event_name,
        img_id: e.event_img,
        event_id: e.event_id,
        event_name: e.event_name,
        event_fee: e.event_fee,
        event_start: e.event_start
      };
    });
    return {
      statusCode: 200,
      body: JSON.stringify(response)
    };
  }catch (e){
    console.log(e);
    return {
      statusCode: 500,
      body: "Internal Server Error"
    };
  }
  

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
