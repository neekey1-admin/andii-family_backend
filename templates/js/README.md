# NodeJSでの書き方

## serverless.yml

```yaml
service: nodejs
useDotenv: true


frameworkVersion: '2'

provider:
  name: aws
  runtime: nodejs12.x
  lambdaHashingVersion: 20201221
  stage: ${opt:stage, self:custom.defaultStage}
  region: ap-northeast-1
  profile: caregiver
  vpc:
    securityGroupIds:
      - ${env:SECURITY_GROUP_ID}
    subnetIds:
      - ${env:SUBNET_ID_1A}
      - ${env:SUBNET_ID_1C}
    
  iamRoleStatements:
    - Effect: Allow
      Action:
        - secretsmanager:GetSecretValue
        - rds-data:*
        - ec2:CreateNetworkInterface
        - ec2:DescribeNetworkInterfaces
        - ec2:DeleteNetworkInterface
        
      Resource: "*"

plugins:
 - serverless-offline
 - serverless-dotenv-plugin

custom:
  defaultStage: development
  prefix: Andii-family
  suffix: ${opt:stage, self:custom.defaultStage}


```

基本系はこれ。あとは、Functionを形成してあげればよくて、

```yaml


functions:
  getEventList:
    handler: handler.get_event_list
    name: ${self:custom.prefix}_get_event_list_${self:custom.suffix}
    memorySize: 1024
    environment:
      POSTGRES_HOST: ${env:POSTGRES_HOST}
      POSTGRES_PORT: ${env:POSTGRES_PORT}
      POSTGRES_PASSWORD: ${env:POSTGRES_PASSWORD}
      POSTGRES_USER: ${env:POSTGRES_USER}
      POSTGRES_DB: ${env:POSTGRES_DB}
    events:
      - http:
          path: event/list/{facility_id}
          method: get
          request:
            parameters:
              paths:
                facility_id: true

```

というような感じにする。  

## nodejs

nodejsでは、以下のように構築する。Handlerの場合

```javascript

'use strict';
const {Client} = require('pg');

module.exports.get_event_list = async (event) => {
  return {
      statusCode: 200,
      body: "OK"
  };
};

```

という具合。ここから作っていけばいい。

## 単体テスト

準備中...
